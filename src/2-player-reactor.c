#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 16000000
#include <util/delay.h>
#include <stdlib.h>

#include "lcd.h"

#define PLAYER1 0
#define PLAYER2 1
#define BOTH_PLAYERS 2
#define LEFT_BUTTON 0
#define RIGHT_BUTTON 1
#define BOTH_BUTTONS 2
#define BUTTON_ENABLED 1
#define BUTTON_DISABLED 0
#define RED_POSITION 0
#define GREEN_POSITION 1
#define BLUE_POSITION 2
#define RED 1
#define BLUE 2
#define GREEN 4
#define LED_ON 1
#define LED_OFF 0
#define NO_GAME 0
#define POST_GAME 1
#define LED_GAME 2
#define CHAR_GAME 3
#define QUESTION_GAME 3

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

uint32_t progress_line1_old = 0;
uint32_t progress_line2_old = 0;

char p1lost[] = "Player1 lost";
char p2lost[] = "Player2 lost";
char p1won[] = "Player1 won";
char p2won[] = "Player2 won";
char led_game_req1[] = "Press ";
char led_game_req2[] = " button";
char led_game_req3[] = "when LED is ";
char red[] = "red";
char green[] = "green";
char blue[] = "blue";
char p1[] = "P1 ";
char p2[] = "P2 ";
char slower[] = "ms slower";

char p1_buttons_enabled[2] = {0, 0};
char p2_buttons_enabled[2] = {0, 0};
char last_led_colour[3] = {0, 0, 0};
char last_led_state[3] = {0, 0, 0};

char selected_colour = 0;
char counter = 0;
char req_counter = 0;
char current_game = NO_GAME;
char requirement = 0;
char start_game = 0;
char selected_button = 3;
char post_game_counter = 0;
char first_player_pressed = 0;
int time = 0;

void clear_screen();
void print_requirement(char game);

void led_ctrl(char player, char colour);
void timer1_start();
void timer1_reset();
void timer1_stop();
void random_led_colour(char except);
void buttons_ctrl(char player, char which, char state);
void button_pressed(char player, char which);

void led_game(); 

ISR(PCINT1_vect)
{
	PCICR &= ~(1 << PCIE1);
	
    if ((PINB & (1 << PB0)) == 0) {
		button_pressed(PLAYER1, LEFT_BUTTON);
	}
		
    if ((PINB & (1 << PB4)) == 0) {
		button_pressed(PLAYER1, RIGHT_BUTTON);
	}
}

ISR(PCINT3_vect)
{
	PCICR &= ~(1 << PCIE3);

	if ((PIND & (1 << PD0)) == 0) {
		button_pressed(PLAYER2, LEFT_BUTTON);
	}
		
    if ((PIND & (1 << PD4)) == 0) {
		button_pressed(PLAYER2, RIGHT_BUTTON);
	}
}

ISR(TIMER1_COMPA_vect)
{
    if (current_game == NO_GAME) {
		
	} else if (current_game == LED_GAME) {
		if (requirement) {
			req_counter--;
			
			if (req_counter == 0) {
				clear_screen();
				requirement = 0;
				PCICR |= (1 << PCIE1);
				PCICR |= (1 << PCIE3);
			}
				
		} else if (counter == 0) {
			led_ctrl(PLAYER1, last_led_colour[BOTH_PLAYERS]);
			led_ctrl(PLAYER2, last_led_colour[BOTH_PLAYERS]);
			
			led_ctrl(PLAYER1, selected_colour);
			led_ctrl(PLAYER2, selected_colour);
			
			last_led_colour[BOTH_PLAYERS] = selected_colour;
			current_game = NO_GAME;
			clear_screen();
		} else {	
			random_led_colour(selected_colour);
			counter--;
		}
	} else if (current_game == POST_GAME) {
		post_game_counter++;
	}
}

void PCINT_init()
{
    PCICR |= (1 << PCIE1);
    PCMSK1 |= (1 << PCINT8) | (1 << PCINT12);
	
	PCICR |= (1 << PCIE3);
    PCMSK3 |= (1 << PCINT24) | (1 << PCINT28);
}
void IO_init()
{
    /* LED-ul este pus ca iesire */
    DDRB |= ((1 << PB1) | (1 << PB2) | (1 << PB3));
	DDRD |= ((1 << PD1) | (1 << PD2) | (1 << PD5)); // !!!!!!
    /* butoanele sunt puse ca intrare */
    DDRB &= ~((1 << PB0) | (1 << PB4));
    PORTB |= ((1 << PB0) | (1 << PB4));
	DDRD &= ~((1 << PD0) | (1 << PD4));
    PORTD |= ((1 << PD0) | (1 << PD4));
}

/* initializeaza timer-ul 1 */
void timer1_init()
{
    TCCR1B |= (1 << CS12)| (1 << CS10) | (1 << WGM12);

    TIMSK1 |= (1 << OCIE1A);
    OCR1A = 15625;
}

int main(void)
{

	timer1_init();
    IO_init();
    LCD_init();
    // LCD_generate_bars();

    PCINT_init();
    sei();
	
	timer1_stop();
	
	srand(TCNT1);
	led_game();

    while (1)
    {

    }
	
    return 0;
}

void led_game()
{	
	current_game = LED_GAME;
	
	selected_colour = (1 << (rand() % 3));
	counter = rand() % 5 + 3;
	counter += counter;
	selected_button = rand() % 2;
	
	print_requirement(LED_GAME);
	timer1_start();
}

void random_led_colour(char except)
{
	char x;
	
	/* daca LED-ul era pornit, il stinge */
	if (last_led_state[BOTH_PLAYERS]) {
		led_ctrl(PLAYER1, last_led_colour[BOTH_PLAYERS]);
		led_ctrl(PLAYER2, last_led_colour[BOTH_PLAYERS]);
		
		last_led_state[BOTH_PLAYERS] = LED_OFF;
		last_led_colour[BOTH_PLAYERS] = 0;
	/* iar daca era stins il aprinde */
	} else {
		do {
			x = rand() % 7 + 1;
		} while (x == last_led_colour[BOTH_PLAYERS] || x == except);
		
		led_ctrl(PLAYER1, x);
		led_ctrl(PLAYER2, x);
		
		last_led_state[BOTH_PLAYERS] = LED_ON;
		last_led_colour[BOTH_PLAYERS] = x;
	}
}

void buttons_ctrl(char player, char which, char state)
{
	switch (player) {
	case PLAYER1:
		if (which == BOTH_BUTTONS) {
			p1_buttons_enabled[LEFT_BUTTON] = state;
			p1_buttons_enabled[RIGHT_BUTTON] = state;
		} else {
			p1_buttons_enabled[(int)which] = state;
		}
		break;
		
	case PLAYER2:
		if (which == BOTH_BUTTONS) {
			p2_buttons_enabled[LEFT_BUTTON] = state;
			p2_buttons_enabled[RIGHT_BUTTON] = state;
		} else {
			p2_buttons_enabled[(int)which] = state;
		}
		break;
		
	default:
		break;
	}
}

void led_ctrl(char player, char colour)
{
	switch (player) {
	case PLAYER1:
		last_led_colour[PLAYER1] = colour;
	
		if (CHECK_BIT(colour, BLUE_POSITION)) 
			PORTB ^= (1 << PB3);
			
		if (CHECK_BIT(colour, GREEN_POSITION))
			PORTB ^= (1 << PB2);
			
		if (CHECK_BIT(colour, RED_POSITION))
			PORTB ^= (1 << PB1);
		
		break;
		
	case PLAYER2:
		last_led_colour[PLAYER2] = colour;
	
		if (CHECK_BIT(colour, BLUE_POSITION)) 
			PORTD ^= (1 << PD5);
			
		if (CHECK_BIT(colour, GREEN_POSITION))
			PORTD ^= (1 << PD2);
			
		if (CHECK_BIT(colour, RED_POSITION))
			PORTD ^= (1 << PD1);
		
		break;
		
	default:
		break;
	}
}

void timer1_start()
{
	TIMSK1 |= (1 << OCIE1A);
}

void timer1_reset()
{
	TCNT1 = 0;
}

void timer1_stop()
{
	TIMSK1 &= ~(1 << OCIE1A);
}

void clear_screen()
{
	char i;
	
	for (i = 0; i < 16; i++) {
		LCD_putCharAt(i, ' ');
		LCD_putCharAt(i + 40, ' ');
	}
}

void print_requirement(char game)
{
	clear_screen();
	
	if (game == LED_GAME) {
		LCD_printAt(0, led_game_req1);
		LCD_putChar(selected_button ? 'R' : 'L');
		LCD_print(led_game_req2);
		LCD_printAt(40, led_game_req3);
		
		if (selected_colour == 1)
			LCD_print(red);
		else if (selected_colour == 2)
			LCD_print(green);
		else
			LCD_print(blue);
	}
	
	req_counter = 5;
	requirement = 1;
}

void LCD_printTime(char player, int time) {
	int x;
	
	x = 0;
	LCD_printAt(40, player == PLAYER1 ? p1 : p2);

	while (time != 0) {
		x = x * 10 + time % 10;
		time /= 10;
	}
	
	while (x != 0) {
		LCD_putChar('0' + x % 10);
		x /= 10;
	}

	LCD_print(slower);
}

void button_pressed(char player, char which)
{	
	if (last_led_colour[BOTH_PLAYERS] == selected_colour &&
			selected_button == which) {
		if (first_player_pressed == 0) {
			LCD_printAt(0, player == PLAYER1 ? p1won : p2won);
			first_player_pressed = 1;
			post_game_counter = 0;
			current_game = POST_GAME;
		} else {
			time = post_game_counter * 1000 + TCNT1 / 500;
			LCD_printTime(player, time);
		}
	} else {
		LCD_printAt(0, player == PLAYER1 ? p1lost : p2lost);
	}
}

